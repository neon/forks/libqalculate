Source: libqalculate
Section: math
Priority: optional
Maintainer: Vincent Legout <vlegout@debian.org>
Build-Depends: debhelper (>= 9), libncurses5-dev, libreadline-dev, libglib2.0-dev, libxml2-dev, libcln-dev (>> 1.2), libgmp-dev, libxml-parser-perl, dh-autoreconf, intltool
Standards-Version: 3.9.8
Homepage: http://qalculate.sourceforge.net/

Package: qalc
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: libgnomevfs2-common | wget
Description: Powerful and easy to use command line calculator
 Qalculate! is small and simple to use but with much power and versatility
 underneath.  Features include customizable functions, units, arbitrary
 precision using a one-line fault-tolerant expression entry.
 .
 This package contains the command line version of Qalculate!.

Package: libqalculate6
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}, libqalculate6-data
Breaks: qalc (<< 0.9.7-2)
Replaces: qalc (<< 0.9.7-2)
Description: Powerful and easy to use desktop calculator - library
 Qalculate! is small and simple to use but with much power and versatility
 underneath.  Features include customizable functions, units, arbitrary
 precision, plotting, and a graphical interface that uses a one-line
 fault-tolerant expression entry (although it supports optional traditional
 buttons).
 .
 This package contains the shared library used by all Qalculate! frontends.

Package: libqalculate6-data
Section: libs
Architecture: all
Multi-Arch: foreign
Depends: ${shlibs:Depends}, ${misc:Depends}
Breaks: libqalculate5 (<< 0.9.7-7), libqalculate5-data
Replaces: libqalculate5 (<< 0.9.7-7), libqalculate5-data
Description: Powerful and easy to use desktop calculator - data
 Qalculate! is small and simple to use but with much power and versatility
 underneath.  Features include customizable functions, units, arbitrary
 precision, plotting, and a graphical interface that uses a one-line
 fault-tolerant expression entry (although it supports optional traditional
 buttons).
 .
 This package contains the data files needed by libqalculate.

Package: libqalculate-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libqalculate6 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}, libcln-dev (>> 1.2), libxml2-dev
Description: Powerful and easy to use desktop calculator - development
 Qalculate! is small and simple to use but with much power and versatility
 underneath.  Features include customizable functions, units, arbitrary
 precision, plotting, and a graphical interface that uses a one-line
 fault-tolerant expression entry (although it supports optional traditional
 buttons).
 .
 This package contains the development files needed to build the frontends.

Package: libqalculate-doc
Section: doc
Architecture: all
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Powerful and easy to use desktop calculator - documentation
 Qalculate! is small and simple to use but with much power and versatility
 underneath.  Features include customizable functions, units, arbitrary
 precision, plotting, and a graphical interface that uses a one-line
 fault-tolerant expression entry (although it supports optional traditional
 buttons).
 .
 This package contains the libqalculate documentation for developers.
